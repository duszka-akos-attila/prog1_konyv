#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define speed 1

void draw(int x,int y,char b){
    
    system("clear");

    int i;

    for(i=0;i<x;i++){
        printf("\n");
    }

    for(i=0;i<y;i++){
        printf(" ");
    }

    printf("%c\n", b);
}

int main(){
    
    int x = 0;
    int y = 0;
    int xspeed=speed;
    int yspeed=speed;
    int width;
    int height;

    printf("Give in the width and the height of the field (in this order): \n");
    scanf("%i %i", &height, &width);


    while(1){

        draw (x, y, "O");

        usleep (50000);

        x = x + xspeed;
        y = y + yspeed;

        if(x>=width-1){
            xspeed = xspeed * -1;
        }
        if(x<=0){
            xspeed = xspeed * -1;
        }
        if(y<=0) { 
            yspeed = yspeed * -1;
        }
        if(y>=height-1){
            yspeed = yspeed * -1;
        }

    }

    return 0;
}
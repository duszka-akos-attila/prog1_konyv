%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>

  #define LEXERSIZE (sizeof lexer / sizeof (struct cipher))
    
  struct cipher {
    char c;
    char *leet[1];
  } lexer [] = {

  {'a', {"4"}},
  {'b', {"8"}},
  {'c', {"("}},
  {'d', {"|)"}},
  {'e', {"3"}},
  {'f', {"1="}},
  {'g', {"6"}},
  {'h', {"|-|"}},
  {'i', {"1"}},
  {'j', {"7"}},
  {'k', {"1<"}},
  {'l', {"|_"}},
  {'m', {"|\\/|"}},
  {'n', {"/V"}},
  {'o', {"0"}},
  {'p', {"|0"}},
  {'q', {"9"}},
  {'r', {"12"}},
  {'s', {"5"}},
  {'t', {"+"}},
  {'u', {"v"}},
  {'v', {"u"}},
  {'w', {"uu"}},
  {'x', {"k"}},
  {'y', {"v\\"}},
  {'z', {"2"}},
  
  {'0', {"O"}},
  {'1', {"I"}},
  {'2', {"Z"}},
  {'3', {"E"}},
  {'4', {"h"}},
  {'5', {"S"}},
  {'6', {"b"}},
  {'7', {"T"}},
  {'8', {"X"}},
  {'9', {"g"}}
  };
  
%}
%%
. {
    
    int found = 0;
    for(int i=0; i<LEXERSIZE; ++i)
    {
    
      if(lexer[i].c == tolower(*yytext))
      {
      
        int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
      
          if(r<91)
          printf("%s", lexer[i].leet[0]);
          else if(r<95)
          printf("%s", lexer[i].leet[1]);
        else if(r<98)
          printf("%s", lexer[i].leet[2]);
        else 
          printf("%s", lexer[i].leet[3]);

        found = 1;
        break;
      }
      
    }
    
    if(!found)
       printf("%c", *yytext);   
    
  }
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}


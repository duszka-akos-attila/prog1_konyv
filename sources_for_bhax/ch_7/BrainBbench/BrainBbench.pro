QT += widgets core
CONFIG += c++11 c++14 c++17
QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp 
LIBS += pkg-config --libs opencv

TEMPLATE = app
TARGET = BrainB
INCLUDEPATH += .

HEADERS += BrainBThread.h  BrainBWin.h
SOURCES += BrainBThread.cpp  BrainBWin.cpp  main.cpp

public class Sejtautomata extends java.awt.Frame implements Runnable {
    
    public static final boolean ELO = true;
    
    public static final boolean HALOTT = false;
    
    protected boolean [][][] racsok = new boolean [2][][];

    protected boolean [][] racs;

    protected int racsIndex = 0;
    protected int cellaSzelesseg = 20;
    protected int cellaMagassag = 20;
    protected int szelesseg = 20;
    protected int magassag = 10;
    protected int varakozas = 1000;
    private java.awt.Robot robot;
    private boolean pillanatfelvetel = false;
    private static int pillanatfelvetelSzamlalo = 0;


    public Sejtautomata(int szelesseg, int magassag) {
        this.szelesseg = szelesseg;
        this.magassag = magassag;

        racsok[0] = new boolean[magassag][szelesseg];
        racsok[1] = new boolean[magassag][szelesseg];
        racsIndex = 0;
        racs = racsok[racsIndex];

        for(int i=0; i<racs.length; ++i)
            for(int j=0; j<racs[0].length; ++j)
                racs[i][j] = HALOTT;

        sikloKilovo(racs, 5, 60);

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                setVisible(false);
                System.exit(0);
            }
        });

        addKeyListener(new java.awt.event.KeyAdapter() {

            public void keyPressed(java.awt.event.KeyEvent e) {
                if(e.getKeyCode() == java.awt.event.KeyEvent.VK_K) {

                    cellaSzelesseg /= 2;
                    cellaMagassag /= 2;
                    setSize(Sejtautomata.this.szelesseg*cellaSzelesseg,
                            Sejtautomata.this.magassag*cellaMagassag);
                    validate();
                } else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_N) {

                    cellaSzelesseg *= 2;
                    cellaMagassag *= 2;
                    setSize(Sejtautomata.this.szelesseg*cellaSzelesseg,
                            Sejtautomata.this.magassag*cellaMagassag);
                    validate();
                } else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_S)
                    pillanatfelvetel = !pillanatfelvetel;
                else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_G)
                    varakozas /= 2;
                else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_L)
                    varakozas *= 2;
                repaint();
            }
        });

        addMouseListener(new java.awt.event.MouseAdapter() {

            public void mousePressed(java.awt.event.MouseEvent m) {

                int x = m.getX()/cellaSzelesseg;
                int y = m.getY()/cellaMagassag;
                racsok[racsIndex][y][x] = !racsok[racsIndex][y][x];
                repaint();
            }
        });

        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {

            public void mouseDragged(java.awt.event.MouseEvent m) {
                int x = m.getX()/cellaSzelesseg;
                int y = m.getY()/cellaMagassag;
                racsok[racsIndex][y][x] = ELO;
                repaint();
            }
        });

        cellaSzelesseg = 10;
        cellaMagassag = 10;

        try {
            robot = new java.awt.Robot(
                    java.awt.GraphicsEnvironment.
                    getLocalGraphicsEnvironment().
                    getDefaultScreenDevice());
        } catch(java.awt.AWTException e) {
            e.printStackTrace();
        }

        setTitle("Sejtautomata");
        setResizable(false);
        setSize(szelesseg*cellaSzelesseg,
                magassag*cellaMagassag);
        setVisible(true);

        new Thread(this).start();
    }

    public void paint(java.awt.Graphics g) {

        boolean [][] racs = racsok[racsIndex];

        for(int i=0; i<racs.length; ++i) {
            for(int j=0; j<racs[0].length; ++j) {

                if(racs[i][j] == ELO)
                    g.setColor(java.awt.Color.BLACK);
                else
                    g.setColor(java.awt.Color.WHITE);
                g.fillRect(j*cellaSzelesseg, i*cellaMagassag,
                        cellaSzelesseg, cellaMagassag);

                g.setColor(java.awt.Color.LIGHT_GRAY);
                g.drawRect(j*cellaSzelesseg, i*cellaMagassag,
                        cellaSzelesseg, cellaMagassag);
            }
        }

        if(pillanatfelvetel) {

            pillanatfelvetel = false;
            pillanatfelvetel(robot.createScreenCapture
                    (new java.awt.Rectangle
                    (getLocation().x, getLocation().y,
                    szelesseg*cellaSzelesseg,
                    magassag*cellaMagassag)));
        }
    }

    public int szomszedokSzama(boolean [][] racs,
            int sor, int oszlop, boolean allapot) {        
        int allapotuSzomszed = 0;

        for(int i=-1; i<2; ++i)
            for(int j=-1; j<2; ++j)

                if(!((i==0) && (j==0))) {

            int o = oszlop + j;
            if(o < 0)
                o = szelesseg-1;
            else if(o >= szelesseg)
                o = 0;
            
            int s = sor + i;
            if(s < 0)
                s = magassag-1;
            else if(s >= magassag)
                s = 0;
            
            if(racs[s][o] == allapot)
                ++allapotuSzomszed;
                }
        
        return allapotuSzomszed;
    }

    public void idoFejlodes() {
        
        boolean [][] racsElotte = racsok[racsIndex];
        boolean [][] racsUtana = racsok[(racsIndex+1)%2];
        
        for(int i=0; i<racsElotte.length; ++i) { 
            for(int j=0; j<racsElotte[0].length; ++j) { 
                
                int elok = szomszedokSzama(racsElotte, i, j, ELO);
                
                if(racsElotte[i][j] == ELO) {

                    if(elok==2 || elok==3)
                        racsUtana[i][j] = ELO;
                    else
                        racsUtana[i][j] = HALOTT;
                }  else {

                    if(elok==3)
                        racsUtana[i][j] = ELO;
                    else
                        racsUtana[i][j] = HALOTT;
                }
            }
        }
        racsIndex = (racsIndex+1)%2;
    }

    public void run() {
        
        while(true) {
            try {
                Thread.sleep(varakozas);
            } catch (InterruptedException e) {}
            
            idoFejlodes();
            repaint();
        }
    }
    

    public void siklo(boolean [][] racs, int x, int y) {
        
        racs[y+ 0][x+ 2] = ELO;
        racs[y+ 1][x+ 1] = ELO;
        racs[y+ 2][x+ 1] = ELO;
        racs[y+ 2][x+ 2] = ELO;
        racs[y+ 2][x+ 3] = ELO;
        
    }
    

    public void sikloKilovo(boolean [][] racs, int x, int y) {
        
        racs[y+ 6][x+ 0] = ELO;
        racs[y+ 6][x+ 1] = ELO;
        racs[y+ 7][x+ 0] = ELO;
        racs[y+ 7][x+ 1] = ELO;
        
        racs[y+ 3][x+ 13] = ELO;
        
        racs[y+ 4][x+ 12] = ELO;
        racs[y+ 4][x+ 14] = ELO;
        
        racs[y+ 5][x+ 11] = ELO;
        racs[y+ 5][x+ 15] = ELO;
        racs[y+ 5][x+ 16] = ELO;
        racs[y+ 5][x+ 25] = ELO;
        
        racs[y+ 6][x+ 11] = ELO;
        racs[y+ 6][x+ 15] = ELO;
        racs[y+ 6][x+ 16] = ELO;
        racs[y+ 6][x+ 22] = ELO;
        racs[y+ 6][x+ 23] = ELO;
        racs[y+ 6][x+ 24] = ELO;
        racs[y+ 6][x+ 25] = ELO;
        
        racs[y+ 7][x+ 11] = ELO;
        racs[y+ 7][x+ 15] = ELO;
        racs[y+ 7][x+ 16] = ELO;
        racs[y+ 7][x+ 21] = ELO;
        racs[y+ 7][x+ 22] = ELO;
        racs[y+ 7][x+ 23] = ELO;
        racs[y+ 7][x+ 24] = ELO;
        
        racs[y+ 8][x+ 12] = ELO;
        racs[y+ 8][x+ 14] = ELO;
        racs[y+ 8][x+ 21] = ELO;
        racs[y+ 8][x+ 24] = ELO;
        racs[y+ 8][x+ 34] = ELO;
        racs[y+ 8][x+ 35] = ELO;
        
        racs[y+ 9][x+ 13] = ELO;
        racs[y+ 9][x+ 21] = ELO;
        racs[y+ 9][x+ 22] = ELO;
        racs[y+ 9][x+ 23] = ELO;
        racs[y+ 9][x+ 24] = ELO;
        racs[y+ 9][x+ 34] = ELO;
        racs[y+ 9][x+ 35] = ELO;
        
        racs[y+ 10][x+ 22] = ELO;
        racs[y+ 10][x+ 23] = ELO;
        racs[y+ 10][x+ 24] = ELO;
        racs[y+ 10][x+ 25] = ELO;
        
        racs[y+ 11][x+ 25] = ELO;
        
    }

    public void pillanatfelvetel(java.awt.image.BufferedImage felvetel) {

        StringBuffer sb = new StringBuffer();
        sb = sb.delete(0, sb.length());
        sb.append("sejtautomata");
        sb.append(++pillanatfelvetelSzamlalo);
        sb.append(".png");

        try {
            javax.imageio.ImageIO.write(felvetel, "png",
                    new java.io.File(sb.toString()));
        } catch(java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public void update(java.awt.Graphics g) {
        paint(g);
    }    

    public static void main(String[] args) {

        new Sejtautomata(100, 75);
    }
}                

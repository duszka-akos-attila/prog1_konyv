import java.io.*;

public class Exor {
    
    public Exor(String keyText,
            java.io.InputStream IStream,
            java.io.OutputStream OStream)
            throws java.io.IOException {
        
        byte [] key = keyText.getBytes();
        byte [] buffer = new byte[256];
        int keyIndex = 0;
        int readBytes = 0;

        while((readBytes =
                IStream.read(buffer)) != -1) {
            
            for(int i=0; i<readBytes; ++i) {
                
                buffer[i] = (byte)(buffer[i] ^ key[keyIndex]);
                keyIndex = (keyIndex+1) % key.length;
                
            }
            
            OStream.write(buffer, 0, readBytes);
            
        }
        
    }
    
    public static void main(String[] args) {
        
        try {
            
            new Exor(args[0], System.in, System.out);
            
        } catch(java.io.IOException e) {
            
            e.printStackTrace();
            
        }
        
    }
    
}

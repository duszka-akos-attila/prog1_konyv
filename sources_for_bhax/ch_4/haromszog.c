#include <stdio.h>
#include <stdlib.h>

int
main ()
{
    int hmb = 5;
    double **tm;
    
    if ((tm = (double **) malloc (hmb * sizeof (double *))) == NULL)
    {
        return -1;
    }
    
    for (int i = 0; i < hmb; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }

    }
    
    for (int i = 0; i < hmb; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < hmb; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    tm[3][0] = 32.0;
    (*(tm + 3))[1] = 43.0;	
    *(tm[3] + 2) = 46.0;
    *(*(tm + 3) + 3) = 65.0;

    for (int i = 0; i < hmb; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < hmb; ++i)
        free (tm[i]);

    free (tm);

    return 0;
}

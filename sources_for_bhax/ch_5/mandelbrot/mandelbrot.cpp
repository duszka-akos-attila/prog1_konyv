#include <png++/png.hpp>

#define N 500
#define M 500
#define MAXX 0.7
#define MINX -2.0
#define MAXY 1.35
#define MINY -1.35

using namespace png;

void GeneratePNG( int pixels[N][M])
{
    image< rgb_pixel > image(N, M);
    for (int x = 0; x < N; x++)
    {
        for (int y = 0; y < M; y++)
        {
            image[x][y] = rgb_pixel(pixels[x][y], pixels[x][y], pixels[x][y]);
        }
    }
    image.write("mandel.png");
}

struct Complex
{
    double re, im;
};

int main()
{
    int pixels[N][M];

    int i, j, k;

    double dx = (MAXX - MINX) / N;
    double dy = (MAXY - MINY) / M;

    struct Complex C, Z, Za;

    int iteration;

    for (i = 0; i < M; i++)
    {
        for (j = 0; j < N; j++)
        {
            C.re = MINX + j * dx;
            C.im = MAXY - i * dy;

            Z.re = 0;
            Z.im = 0;
            iteration = 0;

            while(Z.re * Z.re + Z.im * Z.im < 4 && iteration++ < 255)
            {
                Za.re = Z.re * Z.re - Z.im * Z.im + C.re;
                Za.im = 2 * Z.re * Z.im + C.im;
                Z.re = Za.re;
                Z.im = Za.im;
            }

            pixels[i][j] = 256 - iteration;
        }
    }

    GeneratePNG(pixels);

    return 0;
}

#include "Csomopont.h"

Csomopont *Csomopont::nullasGyermek () const
        {
            return balNulla;
        }
        // Aktuális csomópon,t mondd meg nékem, ki a jobb oldali gyermeked?
        Csomopont *Csomopont::egyesGyermek () const
        {
            return jobbEgy;
        }
        // Aktuális csomópont, ímhol legyen a "gy" mutatta csomópont a bal oldali gyereked!
        void Csomopont::ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }
        // Aktuális csomópont, ímhol legyen a "gy" mutatta csomópont a jobb oldali gyereked!
        void Csomopont::ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }
        // Aktuális csomópont: Te milyen betűt hordozol?
        // (a const kulcsszóval jelezzük, hogy nem bántjuk a példányt)
        char Csomopont::getBetu () const
        {
            return betu;
        }